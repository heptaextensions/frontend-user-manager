<?php

defined('_JEXEX') or die;

class plgInstallerHeptaExtensions extends JPlugin
{
  
	private $loadlanguage = true;
  
	public function onInstallerBeforePackageDownload(&$url,&$headers)
	{
		$myUrl = new JUri();
		$myUrl->parse($url);
      
		if ( parse_url( $myUrl->getHost() == 'extensions.hepta.es' )
        {
			$downloadid = $this->params->get('downloadid','');
			if ($downloadid)
			{
				$myUrl->setVar('dli',$downloadid);
				$url = $myUrl->toString();
			}
			else
			{
              JFactory::getApplication()->enqueueMessage(JText::_('PLG_INSTALLER_HEPTA_NEED_A_DOWNLOAD_KEY'),'notice');    
			}
        }
    }


}