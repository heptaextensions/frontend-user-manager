#!/bin/sh
find ./ -type f -name *.zip | xargs rm -f
VERSION=`xml_grep version ../component/frontendusermanager.xml --text`
rsync -av --progress ../ ./ --exclude build --exclude ".git"
mv component/

#VERSION=`git rev-parse --short HEAD`

zip  -r --exclude=*.svn* "com_frontendusermanager_$VERSION.zip" component

find . \! -name "createpackage.sh" \! -name "com_frontendusermanager_$VERSION.zip" | xargs rm -rf


#!/bin/sh
find ./ -type f -name *.zip | xargs rm -f
VERSION=`xml_grep version ../package/pkg_frontendusermanager.xml --text`
PACKAGE_NAME="frontendusermanager_${VERSION}.zip"
SOURCEDIR=`pwd`"/source"
PACKAGESDIR=`pwd`"/packages"
HEPTALIBRARIES="/home/carcam/repositorios/git/hepta/libraries"
BUILDDIR=`pwd`
mkdir $SOURCEDIR
mkdir $PACKAGESDIR
rsync -av --progress ../ $SOURCEDIR --exclude build --exclude='.git/'
# Get Libraries
mkdir ${SOURCEDIR}/libraries
cd ${SOURCEDIR}/libraries
cp -r ${HEPTALIBRARIES}/parsecsv/library ./parsecsv
zip  -r --exclude=*.svn* "$PACKAGESDIR/lib_parsecsv.zip" parsecsv -x *.svn*

cd $SOURCEDIR/component/
zip  -r $PACKAGESDIR/com_frontendusermanager.zip frontendusermanager.xml script.php administrator site installer languages media site -x *.svn*

cd $SOURCEDIR/package
cp -r * "$PACKAGESDIR"

cd $PACKAGESDIR
zip  -r --exclude=*.svn* "${BUILDDIR}/${PACKAGE_NAME}" installpkg.php pkg_frontendusermanager.xml CHANGELOG.md LICENSE.txt com_frontendusermanager.zip language lib_parsecsv.zip

cd $BUILDDIR

rm -rf $PACKAGESDIR $SOURCEDIR

