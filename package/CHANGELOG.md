## Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

#### [0.0.6] - 2017-01-10
###### Added
- Show package description after installation

###### Changed
- Updated hepta form fields library to latest code

###### Fixed
- Remove testing filter backup and testing filter form the package
- Fixed date ranges in register and last visit filters not to search in the future dates
- Removed 1 week date constrain in the register and last visit filters

#### [0.0.5] - 2017-01-10 (Thanks [Brian](http://brian.teeman.net)!!)
###### Fixed
- Fixing previous package version and install script

#### [0.0.4] - 2017-01-03 (Thanks [Brian](http://brian.teeman.net)!!)
###### Fixed
- Removed testing filter from the package
- Fixed non-defined variables in the layouts

#### [0.0.3] - 2016-10-14
###### Added
- Now you can set which groups should be shown in the view settings.

###### Removed
- Double permission check. Now you only need Frontend User Manager permissions to see the lists

#### [0.0.2] - 2016-09-02
###### Added
- hepta.customdaterange field from Hepta Form Fields Library for easy Custom Date Range selection
- filter_joomlacore.xml form to allow easy filtering of Joomla! Base Fields
- System will read any Joomla! xml form description added into the model and will integrate their fields in the search form
- Allow excluding groups in the listing
- Enabling Live update stream for all users. This will change in the future

###### Changed
- Double the security and user should have permissions to manage Joomla Users component and Frontend User Manager Component

#### [0.0.1] - 2016-05-20
###### Added
- Initial Release

###### Changed
- Initial Release

###### Fixed
- Initial Release
