<?php

/**
 * @version     1.0.0
 * @package     com_frontendusermanager
 * @copyright   Copyright (C) 2015. Joomla Design Studios Inc All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Carlos <carlos@joomladesigner.com> - http://www.joomladesignstudios.com
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');


/**
 * View class for a list of Frontendusermanager.
 */
class FrontendusermanagerViewUsermanagers extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;
    protected $params;
	protected $model;

    /**
     * Display the view
     */
    public function display($tpl = null) {
		$user = JFactory::getUser();
        $app = JFactory::getApplication();

		$authorised = $user->authorise('core.manage', 'com_frontendusermanager');
		if ($authorised !== true)
		{
			throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
		}

        $this->state = $this->get('State');
		$this->pagination = $this->get('Pagination');
        $this->params = $app->getParams('com_frontendusermanager');
		
		$this->items = $this->get('Items');
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
;
            throw new Exception(implode("\n", $errors));
        }

        $this->_prepareDocument();
        parent::display($tpl);
    }

    /**
     * Prepares the document
     */
    protected function _prepareDocument() {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $title = null;
		JHtml::_('jquery.framework');
		
		JHtml::script('com_frontendusermanager/footable/footable.min.js', false, true);
		JHtml::stylesheet('com_frontendusermanager/footable/footable.standalone.min.css', false, true, false, false, true);
		

        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_FRONTENDUSERMANAGER_DEFAULT_PAGE_TITLE'));
        }
        $title = $this->params->get('page_title', '');
        if (empty($title)) {
            $title = $app->getCfg('sitename');
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        $this->document->setTitle($title);

        if ($this->params->get('menu-meta_description')) {
            $this->document->setDescription($this->params->get('menu-meta_description'));
        }

        if ($this->params->get('menu-meta_keywords')) {
            $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
        }

        if ($this->params->get('robots')) {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }

		
    }

	public function loadExtraLanguageFiles($extension)
	{
		$lang = JFactory::getLanguage();
		$languageTag = $lang->getTag();
		$lang->load($extension, JPATH_SITE, $languageTag, true);
	}

}
